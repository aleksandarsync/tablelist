import { Component } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';

// services
import { RecipeService } from './services/recipe-service/recipe.service';

// models
import { RecipePuppy } from './models/recipe-puppy.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  searchTerm$ = new BehaviorSubject<string>('');
  data: RecipePuppy;
  page: number = 1;

  constructor(private recipeService: RecipeService) {
    this.recipeService.search(this.searchTerm$, this.page)
      .subscribe(results => {
        this.data = results;
        this.page++;
      });
  }

  onKeyUp(event): void {
    this.data = undefined;
    this.page = 1;
    this.searchTerm$.next(event.target.value);
  }

  onScroll() {
    this.recipeService.getData(this.searchTerm$.getValue(), this.page)
      .subscribe(data => {
        if (data !== undefined) {
          this.data.results = this.data.results.concat(data['results']);
          this.page++;
        }
      });
  }
}
