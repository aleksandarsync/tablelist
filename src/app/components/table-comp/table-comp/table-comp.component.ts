import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-table-comp',
  templateUrl: './table-comp.component.html',
  styleUrls: ['./table-comp.component.scss']
})
export class TableCompComponent implements OnInit {
  @Input() headerFields: string = 'test, test1, test2';
  @Input() bodyFields: string = '';
  @Input() data: any;

  headerFlds: string[] = [];
  bodyDataMap: string[] = [];

  constructor() { }

  ngOnInit(): void {
    this.headerFlds = this.headerFields.split(',');
    this.bodyDataMap = this.bodyFields.split(',');
  }

}
