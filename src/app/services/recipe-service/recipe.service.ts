import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

const BASE_URL = 'http://www.recipepuppy.com';

@Injectable({
  providedIn: 'root'
})
export class RecipeService {

  constructor(
    private httpClient: HttpClient
  ) { }

  search(terms: Observable<string>, page?: number): Observable<any> {
    return terms.pipe(debounceTime(400),
      distinctUntilChanged(),
      switchMap(term => this.getData(term, page)));
  }

  getData(ingredients: string, page?: number) {
    let res = ingredients.split(' ').join(',').toString();
    return this.httpClient.get(`https://cors-anywhere.herokuapp.com/${BASE_URL}/api/?i=${res}&p=${page}`);
    // return this.httpClient.get(`http://localhost:4200/assets/data.json`);
  }

}
