export interface Recipe {
    title: string;
    href: string;
    ingredients: string;
    thumbnail: string;
}

export interface RecipePuppy {
    title: string;
    version: string;
    href: string;
    results: Recipe[];
}